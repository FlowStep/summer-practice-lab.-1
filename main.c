#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "stream.h"
#include "circular_array.h"
#include "rk.h"
#define MAX_SIZE 256

void delete_line_break(char *s)
{
	while (*s != '\n' && *s != '\0')
		++s;
	*s = '\0';
}

void gets_l(char *s)
{
	gets(s);
	delete_line_break(s);
}

int main()
{
	const static t_vtable tbl = { fwrite, fread, fputc, fgetc, fseek, ftell, feof, ferror, fclose };

	char filename[MAX_SIZE];
	printf("Enter the filename: ");
	gets_l(filename);

	t_stream *fin = new_stream(fopen(filename, "rb"), &tbl);
	if (fin->data == NULL)
	{
		printf("File \"%s\" not found\n", filename);
		system("pause");
		return 1;
	}

	char pattern[MAX_SIZE];
	printf("Enter the pattern: ");
	gets_l(pattern);
	unsigned long long occ = rk_occurrences_count(pattern, fin, (int) 0);
	printf("File %s contains %llu occurrences of pattern \"%s\"\n", filename, occ, pattern);
	off_stream(fin);
	system("pause");
}