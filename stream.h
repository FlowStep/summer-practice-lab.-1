#ifndef __INCLUDE_DATASTREAM_H
#define __INCLUDE_DATASTREAM_H

#include <stdlib.h>
#include <stdio.h>

//Структура таблицы виртуальных функций:
typedef struct t_vtable {
	size_t (*_write) (const void *buff, size_t size, size_t count, void *stream);
	size_t (*_read)  (void *buff, size_t size, size_t count, void *stream);
	int    (*_putc)  (int ch, void *stream);
	int    (*_getc)  (void *stream);
	int    (*_seek)  (void *stream, long offset, int origin);
	long   (*_tell)  (void *stream);
	int    (*_eof)   (void *stream);
	int    (*_error) (void *stream);
	int    (*_close) (void *stream);
	//...
} t_vtable;

//Структура виртуального потока:
typedef struct t_stream {
	const t_vtable *vtbl;
	void *data;
} t_stream;

#ifdef __GNUC__
#define PREFIX static inline
#else
#define PREFIX static
#endif

//Создает виртуальный поток:
PREFIX t_stream *new_stream(void *data, const t_vtable *vtbl) {
	t_stream *stream = (t_stream *) malloc(sizeof(t_stream));
	if (stream == NULL) return NULL;
	stream->data = data;
	stream->vtbl = vtbl;
	return stream;
}

//Выводит данные из буфера в поток:
PREFIX size_t put_stream_buff(const void *buff, size_t size, size_t count, t_stream *stream) {
	return stream->vtbl->_write(buff, size, count, stream->data);
}

//Читает данные из потока в буфер:
PREFIX size_t get_stream_buff(void *buff, size_t size, size_t count, t_stream *stream) {
	return stream->vtbl->_read(buff, size, count, stream->data);
}

//Выводит символ в поток:
PREFIX int put_stream_char(int ch, t_stream *stream) {
	return stream->vtbl->_putc(ch, stream->data);
}

//Читает символ из потока:
PREFIX int get_stream_char(t_stream *stream) {
	return stream->vtbl->_getc(stream->data);
}

//Смещает текущую позицию в потоке:
PREFIX int run_stream_seek(t_stream *stream, long offset, int origin) {
	return stream->vtbl->_seek(stream->data, offset, origin);
}

//Возвращает текущую позицию в потоке:
PREFIX long get_stream_tell(t_stream *stream) {
	return stream->vtbl->_tell(stream->data);
}

//Проверяет окончание потока:
PREFIX int get_stream_eof(t_stream *stream) {
	return stream->vtbl->_eof(stream->data);
}

//Проверяет наличие ошибки:
PREFIX int get_stream_error(t_stream *stream) {
	return stream->vtbl->_error(stream->data);
}

//Закрывает поток:
PREFIX int off_stream(t_stream *stream) {
	if (stream == NULL) return 0;
	stream->vtbl->_close(stream->data);
	free(stream);
	return 0;
}

#endif //__INCLUDE_DATASTREAM_H
