#ifndef __INCLUDE_CIRCULAR_ARRAY_H
#define __INCLUDE_CIRCULAR_ARRAY_H

#include "stream.h"

#define FIRST_I(CA) ca_index(CA, 0)
#define LAST_I(CA) ca_index(CA, CA->size - 1)

typedef struct t_circular_array
{
	int pos;
	int size;
	char *str;
} t_circ_arr;

t_circ_arr *ca_new_ca(int size);
int ca_push_c(char c, t_circ_arr *ca);
void ca_push_s(char *s, t_circ_arr *ca);
int ca_get_and_push_c(t_stream *istream, t_circ_arr *ca);
int ca_get_and_push_s(t_stream *istream, t_circ_arr *ca);
int ca_index(t_circ_arr *ca, int index);
void ca_print_ca(t_circ_arr *ca);

#endif