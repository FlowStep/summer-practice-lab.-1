#include <stdio.h>
#include "stream.h"

typedef unsigned char uchar;

void rle_encode(t_stream *istream, t_stream *ostream)
{
	uchar prev = get_stream_char(istream);
	uchar len = 0;

	while (!get_stream_eof(istream))
	{
		uchar cur = get_stream_char(istream);
		if ((++len == 255) || (cur != prev))
		{
			put_stream_char(prev, ostream);
			put_stream_char(len, ostream);
			len = 0;
		}
		prev = cur;
	}
}

void rle_decode(t_stream *istream, t_stream *ostream)
{
	while (!get_stream_eof(istream))
	{
		uchar c, count;
		c = get_stream_char(istream);
		if (get_stream_eof(istream))
			break;
		count = get_stream_char(istream);
		for (int i = 0; i < count; ++i)
			put_stream_char(c, ostream);
	}
}