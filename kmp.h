#ifndef __INCLUDE_KMP_H
#define __INCLUDE_KMP_H

#include "stream.h"

unsigned long long int kmp_occurrences_count(char *pattern, t_stream *istream, int overlap);

#endif