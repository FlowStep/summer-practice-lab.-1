#include <stdio.h>
#include <stdlib.h>
#include "stream.h"
#include "circular_array.h"

t_circ_arr *ca_new_ca(int size) //hereinafter ca - circular array
{
	t_circ_arr *ca = (t_circ_arr *)malloc(sizeof(t_circ_arr));
	ca->pos = 0;
	ca->size = size;
	ca->str = malloc(size * sizeof(char));
	return ca;
}

int ca_push_c(char c, t_circ_arr *ca)
{
	int ch = ca->str[ca->pos++] = c;
	ca->pos %= ca->size;
	return ch;
}

void ca_push_s(char *s, t_circ_arr *ca)
{
	for (int i = 0; i < ca->size; ++i)
		ca->str[ca_index(ca, i)] = *s++;
}

int ca_get_and_push_c(t_stream *istream, t_circ_arr *ca)
{
	return ca_push_c(get_stream_char(istream), ca);
}

/*Put ca->size simbols from stream or less if end of 
stream is reached and reaturn sum of these simbols*/
int ca_get_and_push_s(t_stream *istream, t_circ_arr *ca)
{
	int count = 0; int sum = 0;
	while (!get_stream_eof(istream))
	{
		sum += ca_get_and_push_c(istream, ca);
		++count;
		if (count == ca->size)
			return sum;
	}
	return sum;
}

int ca_index(t_circ_arr *ca, int index)
{
	return (ca->pos + index) % ca->size;
}

void ca_print_ca(t_circ_arr *ca)
{
	for (int i = ca->pos; i < (ca->pos + ca->size); ++i)
		putchar(ca->str[i % ca->size]);
}