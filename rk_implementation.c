#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stream.h"
#include "circular_array.h"
#define DEVIDER 1000457

typedef struct t_substr
{
	t_circ_arr *ca;
	int hash;
} t_substr;

typedef unsigned long long int UINT64;

t_substr *rk_initialize_str(int len)
{
	t_substr *substr;
	substr = malloc(sizeof(t_substr));
	substr->ca = ca_new_ca(len);
	substr->hash = 0;
	return substr;
}

/*finds sum (hash) of sub_str->ca->str*/
void rk_evaluate_hash(t_substr *sub_str)
{
	for (int i = 0; i < sub_str->ca->size; ++i)
		sub_str->hash += (int)sub_str->ca->str[ca_index(sub_str->ca, i)];
	sub_str->hash %= DEVIDER;
}

/*shifts comparing string to "step" steps returnes 1 if shifting was 
finished succesful and returnes 0 if eof was riched					*/
int rk_move_pattern(t_stream *istream, t_substr *sub_str, int step)
{
	for (int i = 0; i < step; ++i)
	{
		sub_str->hash -= (int)sub_str->ca->str[FIRST_I(sub_str->ca)];
		ca_get_and_push_c(istream, sub_str->ca);
		sub_str->hash += (int)sub_str->ca->str[LAST_I(sub_str->ca)];
		if (get_stream_eof(istream))
			return 0;
	}

	sub_str->hash %= DEVIDER;
	return 1;
}

UINT64 rk_occurrences_count(char *pattern, t_stream *istream, int overlap)
{
	if (*pattern == '\0')
		return 0;
	UINT64 occurence_count = 0;
	int pattern_len = strlen(pattern);
	t_substr *sub_pattern = rk_initialize_str(pattern_len);
	t_substr *sub_str = rk_initialize_str(pattern_len);
	ca_push_s(pattern, sub_pattern->ca);
	ca_get_and_push_s(istream, sub_str->ca);
	rk_evaluate_hash(sub_pattern);
	rk_evaluate_hash(sub_str);

	while (!get_stream_eof(istream))
	{
		if (sub_pattern->hash == sub_str->hash)
			for (int i = 0; i < pattern_len; ++i)
			{
				if (sub_pattern->ca->str[ca_index(sub_pattern->ca, i)] != sub_str->ca->str[ca_index(sub_str->ca, i)])
					break;
				if (i == pattern_len - 1)
				{
					++occurence_count;
					if (!overlap && !rk_move_pattern(istream, sub_str, pattern_len - 1))
						return occurence_count;
					break;
				}
			}
		rk_move_pattern(istream, sub_str, 1);
	} 

	return occurence_count;
}