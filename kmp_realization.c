#define _CRT_SECURE_NO_WARNINGS
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stream.h"
#define MAX_SIZE 256

typedef struct t_str
{
	char *str;
	int *pref;
} t_str;

t_str *kmp_new_str(int len)
{
	t_str *str = malloc(sizeof(t_str));
	str->str = (char *)calloc(len, sizeof(char));
	str->pref = (int *)calloc(len, sizeof(int));
	return str;
}

void kmp_destroy_str(t_str *str)
{
	free(str->str);
	free(str->pref);
	free(str);
}

void kmp_str_copy(t_str *dest, char *src)
{
	strcpy(dest->str, src);
}

void kmp_evaluate_pref(t_str *str, int pref_border)
{
	str->pref[0] = 0;
	int len = strlen(str->str);
	for (int i = 0; i < len - 1; ++i)
	{
		int k = str->pref[i];
		while (k > 0 && str->str[i + 1] != str->str[k])
			k = str->pref[k - 1];
		if (str->str[i + 1] == str->str[k])
			++k;
		if (pref_border != -1 && k > pref_border)
			k = pref_border;
		str->pref[i + 1] = k;
	}
}

unsigned long long int kmp_occurrences_count(char *pattern, t_stream *istream, int overlap)
{
	int pat_len = strlen(pattern);
	t_str *pat = kmp_new_str(pat_len);
	kmp_str_copy(pat, pattern);
	kmp_evaluate_pref(pat, -1);

	int count = 0;

	size_t c;
	int p = 0;

	while ((c = get_stream_char(istream)) != EOF)
	{
		while (p > 0 && (char)c != pat->str[p])
			p = pat->pref[p - 1];
		if (pat->str[p] == (char)c)
			++p;
		if (p == pat_len)
		{
			count++;
			if (overlap)
				p = pat->pref[p - 1];
			else
				p = 0;
		}
	}

	return count;
}