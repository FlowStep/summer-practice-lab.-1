#ifndef __INCLUDE_RLE_H
#define __INCLUDE_RLE_H

#include "stream.h"

void rle_encode(t_stream *istream, t_stream *ostream);
void rle_decode(t_stream *istream, t_stream *ostream);

#endif