#ifndef __INCLUDE_RK_H
#define __INCLUDE_RK_H

#include "stream.h"

unsigned long long int rk_occurrences_count(char *pattern, t_stream *istream, int overlap);

#endif